#!/usr/bin/env bash

DIR=$(pwd)

dockerun(){
    echo --------------------------------------
    echo --------------------------------------
    echo Stop the running image vishwas/crawler
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker stop $(docker ps -q --filter ancestor=vishwas/crawler )

    echo --------------------------------------
    echo --------------------------------------
    echo ---Remove the image vishwas/crawler---
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker rmi -f vishwas/crawler

    echo --------------------------------------
    echo --------------------------------------
    echo ---Build the image vishwas/crawler ---
    echo --------------------------------------
    echo --------------------------------------
    $DIR/mvnw install dockerfile:build

    echo --------------------------------------
    echo --------------------------------------
    echo -------Run the build image------------
    echo --------------------------------------
    echo --------------------------------------
    sleep 2
    docker run -e "SPRING_PROFILES_ACTIVE=prod" -p 8080:8080 -t vishwas/crawler
}

bootrun(){
    echo --------------------------------------
    echo -------------Boot Run-----------------
    echo --------------------------------------
    $DIR/mvnw spring-boot:run
}

stopdocker(){
    echo --------------------------------------
    echo --------------------------------------
    echo Stopping the running image vishwas/crawler
    echo --------------------------------------
    echo --------------------------------------
    docker stop $(docker ps -q --filter ancestor=vishwas/crawler )
}

OPTION=$1

case $OPTION in
dockerun)
    echo "Starting as docker"
    dockerun
    ;;
stopdocker)
    echo "Stop docker"
    stopdocker
    ;;
bootrun)
    echo "Sarting as bootrun"
    bootrun
    ;;
*)
    echo ""
    echo "-- Options --"
    echo "dockerun : Starting as docker. for eg - ./runme.sh dockerun"
    echo "bootrun : Sarting as bootrun. This will open a debug port 48085. for eg - ./runme.sh bootrun"
    echo "stopdocker : Stop docker. for eg - ./runme.sh stopdocker"
esac
