#Objective
The objective is to build a web crawler serving a REST API that fulfills the following
requirements.

• The user requests the analysis of a webpage by supplying a valid URL to the
API created by you

• The user receives the results via the API created by you (preferably JSON)
For a valid URL that returns HTML and which is reachable from the server, the server
should perform the following analysis and compile a report containing:

• Number of hypermedia links in the document, grouped into "internal" links to
the same domain and "external" links to other domains.

• Having collected the links on an HTML page, report for each link if it’s available
via HTTP or HTTPS.

• Consider the effect of redirection and report accordingly

• Collect the results of attempting to reach all the links (either true or
false).

• In the case of an unreachable link, provide some information on what
went wrong.

• Responsiveness of the API is relevant, so please document your
thoughts on implementing it in an efficient way.

#Solution

Below is the tech stack used

Frameworks : Springboot 2.0 used 
https://start.spring.io/

Runtime : Java 8

HTML Parser : jsoup 1.11.3

https://jsoup.org/cookbook/extracting-data/working-with-urls

Build tool : maven 3.3.9

##How to run

It can be simply run as spring bootrun or as docker. 

./runme.sh

-- Options --

dockerun : Starting as docker. for eg - ./runme.sh dockerun

bootrun : Sarting as bootrun. This will open a debug port 48085 for remote debug. for eg - ./runme.sh bootrun

stopdocker : Stop docker. for eg - ./runme.sh stopdocker


**Or** 

A simple mvn spring-boot:run at root directory, would bring up the application.

**Once the application is running using swagger 2 you can test the endpoint directly by "try it out" option** 

http://localhost:8080/swagger-ui.html

username/password - vishwas/vishwas

**postman collection is present at ./postman/scott.postman_collection.json**

##Api details
**/crawler (GET)**

This endpoint would fetch all the the href and groups it based on same domain or different domain or failed. 
```json
{
  "failed": [
    {
      "isredirect": true, /* Returns true if the url redirects */
      "message": "string", /* Reason for failure */
      "protocol": "string", /* http or https */
      "sameDomain": true, /* true if it is same domain */
      "status": 0, /* Status code for the connection */
      "url": "string", /* url tried */
      "valid": true /* true if connection was successful */
    }
  ],
  "otherdomain": [
    {
      "isredirect": true, /* Same as above */
      "message": "string",
      "protocol": "string",
      "sameDomain": true,
      "status": 0,
      "url": "string",
      "valid": true
    }
  ],
  "samedomain": [
    {
      "isredirect": true,/* Same as above */
      "message": "string",
      "protocol": "string",
      "sameDomain": true,
      "status": 0,
      "url": "string",
      "valid": true
    }
  ],
  "title": "string", /* Page title */
  "totalHyperMedialinks": 0, /* Total href on page */
  "url": "string" /* url of the page. */
}
```



curl -X GET "http://localhost:8080/crawler?urlstring=https://github.com" -H "accept: */*"

###Limitations:
- Used Basic security with username and password vishwas/vishwas. Should move to oauth.
- Even though the checks are made asynch. The request is blocked till all the results are fetched.
Should have made to return results as and when the FE requests. FE would be 
polling our api till we send finished flag.
- Packaging, I like the way spring recommends but somehow most of them dont like this,
 https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-structuring-your-code.html 


##Maven
###To check coverage(current coverage is 90%)

$ mvn clean test

view report at 'target/site/jacoco/index.html'

###To check code quality

$ mvn compile site

 view report at target/site/pmd.html

 view report at target/site/spotbugs.html

