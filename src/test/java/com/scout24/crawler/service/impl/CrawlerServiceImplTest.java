package com.scout24.crawler.service.impl;

import com.scout24.crawler.CrawlerApplicationTests;
import com.scout24.crawler.api.dto.CrawlerResponse;
import com.scout24.crawler.config.CrawlerAppProps;
import com.scout24.crawler.service.CheckConnectionAsynch;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URISyntaxException;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(CrawlerApplicationTests.class)
public class CrawlerServiceImplTest {
    CrawlerServiceImpl crawlerService;

    @Before
    public void setUp() {
        CrawlerAppProps crawlerAppProps = new CrawlerAppProps();
        crawlerAppProps.setAbs_href("abs:href");
        crawlerAppProps.setAhref("a[href]");
        crawlerAppProps.setPassword("vishwas");
        crawlerAppProps.setUsername("vishwas");
        crawlerAppProps.setRemove_www_index(4);
        crawlerAppProps.setWww("www");
        crawlerAppProps.setSuccess("Success!!");
        CheckConnectionAsynch checkConnectionAsynch = new CheckConnectionAsynchImpl();
        crawlerService = new CrawlerServiceImpl(checkConnectionAsynch, crawlerAppProps);
    }

    @Test
    public void getUrlAnalysisSuccess() throws IOException, URISyntaxException {
        String url = "https://github.com";
        CrawlerResponse crawlerResponse = crawlerService.getUrlAnalysis(url);
        Assert.assertNotNull(crawlerResponse);
        Assert.assertEquals(crawlerResponse.getUrl(), url);
        Assert.assertTrue(5 < crawlerResponse.getTotalHyperMedialinks());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUrlAnalysisFailed() throws IOException, URISyntaxException {
        CrawlerResponse crawlerResponse = crawlerService.getUrlAnalysis("www.google.com");
    }

    @Test(expected = ConnectException.class)
    public void getUrlAnalysisLocalhostFailed() throws IOException, URISyntaxException {
        CrawlerResponse crawlerResponse = crawlerService.getUrlAnalysis("https://localhost:8080/blabla");
    }
}