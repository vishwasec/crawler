package com.scout24.crawler.api.dto;

import com.scout24.crawler.config.CrawlerAppProps;

import org.junit.Test;

import pl.pojo.tester.api.assertion.Method;

import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class SetterGettersTest {

    @Test
    public void crawlerResponseTest() {
        final Class<?> classUnderTest = CrawlerResponse.class;
        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER, Method.EQUALS, Method.HASH_CODE, Method.TO_STRING, Method.CONSTRUCTOR).areWellImplemented();
    }

    @Test
    public void urlLinksTest() {
        final Class<?> classUnderTest = UrlLinks.class;
        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER, Method.TO_STRING, Method.CONSTRUCTOR).areWellImplemented();
    }

    @Test
    public void crawlerAppPropsTest() {
        final Class<?> classUnderTest = CrawlerAppProps.class;
        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER, Method.EQUALS, Method.HASH_CODE, Method.CONSTRUCTOR).areWellImplemented();
    }
}