package com.scout24.crawler.api;

import com.scout24.crawler.CrawlerApplicationTests;
import com.scout24.crawler.exception.CrawlerErrorHandler;
import com.scout24.crawler.service.CrawlerService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(CrawlerApplicationTests.class)
public class CrawlerControllerTest {
    private MockMvc mvc;

    @Mock
    private CrawlerService crawlerService;
    @InjectMocks
    private CrawlerController crawlerController;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(crawlerController)
                .setControllerAdvice(new CrawlerErrorHandler())
                .build();
    }

    @Test
    public void getUrlAnalysisSuccess() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/crawler?urlstring=www.bitbucket.org/vishwasec/crawler/src/master/")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void getUrlAnalysisFailed() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/crawler")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }
}
