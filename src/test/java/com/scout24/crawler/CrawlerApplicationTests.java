package com.scout24.crawler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrawlerApplicationTests {

    @Value("${crawler.thread.name:Crawler-}")
    private String crwalerThreadName;

    @Value("${crawler.thread.core.poolsize:7}")
    private int corePoolSize;

    @Value("${crawler.thread.maxpoolsize:42}")
    private int maxPoolSize;

    @Value("${crawler.thread.queueCapacity:11}")
    private int queueCapacity;

    @Test
    public void contextLoads() {
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(crwalerThreadName);
        executor.initialize();
        return executor;
    }

}
