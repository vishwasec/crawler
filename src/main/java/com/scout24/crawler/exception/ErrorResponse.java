package com.scout24.crawler.exception;

import io.swagger.annotations.ApiModel;

@ApiModel
public class ErrorResponse {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
