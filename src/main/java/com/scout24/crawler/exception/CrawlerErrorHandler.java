package com.scout24.crawler.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class CrawlerErrorHandler {

    private Map<Object, HttpStatus> exceptionMap = Collections.unmodifiableMap(Stream.of(
            new SimpleEntry<>(IllegalAccessException.class, HttpStatus.FORBIDDEN),
            new SimpleEntry<>(MissingServletRequestParameterException.class, HttpStatus.BAD_REQUEST)
    )
            .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue)));


    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ErrorResponse exceptionHandle(Exception ex, HttpServletResponse response) {
        ErrorResponse res = new ErrorResponse();
        res.setError(ex.getMessage());
        HttpStatus httpStatus = exceptionMap.get(ex.getClass());
        int statusCode = httpStatus != null ? httpStatus.value() : HttpStatus.INTERNAL_SERVER_ERROR.value();
        response.setStatus(statusCode);
        log.error("Failed to process the request. Error code: {}, exception: {}", statusCode, ex);
        return res;
    }
}