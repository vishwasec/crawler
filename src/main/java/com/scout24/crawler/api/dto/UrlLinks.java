package com.scout24.crawler.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlLinks {
    private String protocol;
    private String url;
    private int status;
    private Boolean isredirect;
    private String message;
    private Boolean valid;
    @JsonIgnore
    @JsonProperty("sameDomain")
    private boolean isSameDomain;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UrlLinks))
            return false;
        UrlLinks urlLinks = (UrlLinks) o;
        return Objects.equals(getUrl(), urlLinks.getUrl());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getUrl());
    }
}
