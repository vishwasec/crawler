package com.scout24.crawler.api.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CrawlerResponse {
    private List<UrlLinks> samedomain = new ArrayList<>();
    private List<UrlLinks> otherdomain = new ArrayList<>();
    private List<UrlLinks> failed = new ArrayList<>();
    private String title;
    private String url;
    private Long totalHyperMedialinks;

}
