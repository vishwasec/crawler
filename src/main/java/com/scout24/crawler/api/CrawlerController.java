package com.scout24.crawler.api;

import com.scout24.crawler.api.dto.CrawlerResponse;
import com.scout24.crawler.service.CrawlerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController()
@RequestMapping("/crawler")
@Api(value = "Crawler", description = "Gets detailed analysis of a website.")
public class CrawlerController {
    private final CrawlerService crawlerServiceImpl;

    @Autowired
    public CrawlerController(CrawlerService crawlerServiceImpl) {
        this.crawlerServiceImpl = crawlerServiceImpl;
    }

    @GetMapping
    @ApiOperation(value = "Gets all the href present and reports it accordingly.")
    public ResponseEntity<CrawlerResponse> getUrlAnalysis(@RequestParam String urlstring)
            throws IOException, URISyntaxException {
        return new ResponseEntity<>(crawlerServiceImpl.getUrlAnalysis(urlstring), HttpStatus.OK);
    }

}
