package com.scout24.crawler.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "appprops")
public class CrawlerAppProps {
    private String ahref;
    private String abs_href;
    private String success;
    private String www;
    private int remove_www_index;
    private String username;
    private String password;

}
