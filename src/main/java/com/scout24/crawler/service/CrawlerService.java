package com.scout24.crawler.service;

import com.scout24.crawler.api.dto.CrawlerResponse;

import java.io.IOException;
import java.net.URISyntaxException;

public interface CrawlerService {
    CrawlerResponse getUrlAnalysis(String urlstring) throws IOException, URISyntaxException;

}
