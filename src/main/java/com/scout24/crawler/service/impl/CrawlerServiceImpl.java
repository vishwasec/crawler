package com.scout24.crawler.service.impl;

import com.scout24.crawler.api.dto.CrawlerResponse;
import com.scout24.crawler.api.dto.UrlLinks;
import com.scout24.crawler.config.CrawlerAppProps;
import com.scout24.crawler.service.CheckConnectionAsynch;
import com.scout24.crawler.service.CrawlerService;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CrawlerServiceImpl implements CrawlerService {

    private final CheckConnectionAsynch checkConnectionAsynch;
    private final CrawlerAppProps crawlerAppProps;

    @Autowired
    public CrawlerServiceImpl(CheckConnectionAsynch checkConnectionAsynch, CrawlerAppProps crawlerAppProps) {
        this.checkConnectionAsynch = checkConnectionAsynch;
        this.crawlerAppProps = crawlerAppProps;
    }

    public CrawlerResponse getUrlAnalysis(String urlstring)
            throws IOException, URISyntaxException {
        long startTime = System.currentTimeMillis();

        log.info("Getting results for {}", urlstring);

        CrawlerResponse crawlerResponse = new CrawlerResponse();
        crawlerResponse.setUrl(urlstring);
        final String domainname = getDomainName(new URI(urlstring));
        Document doc = Jsoup.connect(urlstring).get();
        crawlerResponse.setTitle(doc.title());

        // * Creates UrlLinks object with absolute url links for internal domains.
        // * Ignore checks for duplicate url. UrlLinks equals and hashcode have been overridden.
        Set<UrlLinks> urlLinksList = doc.select(crawlerAppProps.getAhref()).parallelStream()
                .filter(links -> !StringUtils.isEmpty(links.attr(crawlerAppProps.getAbs_href())))
                .map(t -> getUrlLinks(t, domainname))
                .collect(Collectors.toSet());

        urlLinksList.parallelStream().peek(checkConnectionAsynch::checkConnection)
                .forEach(urlLinks -> groupResponses(crawlerResponse, urlLinks));

        crawlerResponse.setTotalHyperMedialinks((long) urlLinksList.size());
        long endTime = System.currentTimeMillis();

        log.info("Total time taken {} ms for url {}", (endTime - startTime), urlstring);
        return crawlerResponse;

    }

    private void groupResponses(CrawlerResponse crawlerResponse, UrlLinks urlLinks) {
        log.debug("grouping the responses");

        if (!urlLinks.getMessage().equalsIgnoreCase(crawlerAppProps.getSuccess())) {
            crawlerResponse.getFailed().add(urlLinks);
        }

        if (urlLinks.isSameDomain()) {
            crawlerResponse.getSamedomain().add(urlLinks);
        } else {
            crawlerResponse.getOtherdomain().add(urlLinks);
        }
    }

    private UrlLinks getUrlLinks(Element links, String domainname) {
        log.debug("preparing urlinks for check connections.");

        UrlLinks urlLinks = new UrlLinks();
        urlLinks.setUrl(links.attr(crawlerAppProps.getAbs_href()));
        try {
            URI url = new URI(urlLinks.getUrl());
            urlLinks.setSameDomain(StringUtils.equals(domainname, getDomainName(url)));
            urlLinks.setProtocol(url.getScheme());
            urlLinks.setUrl(url.toString());
            urlLinks.setMessage(crawlerAppProps.getSuccess());
        } catch (Exception e) {
            log.error("Failed to get responses. {} ", e);
            urlLinks.setMessage(e.getMessage());
        }
        return urlLinks;
    }

    private String getDomainName(URI uri) {
        log.debug("Checking if it is same domain {} ", uri);
        String domain = uri.getHost();
        if (StringUtils.isEmpty(domain)) {
            throw new IllegalArgumentException("Url is not valid. Maybe you missed something! " +
                    "for eg. http/https");
        }
        return domain.startsWith(crawlerAppProps.getWww())
                ? domain.substring(crawlerAppProps.getRemove_www_index()) : domain;
    }

}
