package com.scout24.crawler.service.impl;

import com.scout24.crawler.api.dto.UrlLinks;
import com.scout24.crawler.service.CheckConnectionAsynch;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CheckConnectionAsynchImpl implements CheckConnectionAsynch {

    private static final String LOCATION = "location";

    /**
     * Below method checks asynchronously and reports.
     */
    @Async
    @Override
    public void checkConnection(UrlLinks urlLinks) {
        try {
            log.debug("CHecking for url {} ", urlLinks.getUrl());

            Connection.Response response = Jsoup.connect(urlLinks.getUrl()).followRedirects(false).execute();
            urlLinks.setStatus(response.statusCode());
            urlLinks.setIsredirect(response.hasHeader(LOCATION));
            urlLinks.setValid(true);
        } catch (HttpClientErrorException e) {
            log.error("Failed checking availability of url {}", e);
            urlLinks.setStatus(e.getStatusCode().value());
            urlLinks.setMessage(e.getMessage());
            urlLinks.setValid(false);
        } catch (Exception e) {
            log.error("Failed checking availability of url {}", e);
            urlLinks.setMessage(e.getMessage());
            urlLinks.setValid(false);
        }
    }
}
