package com.scout24.crawler.service;

import com.scout24.crawler.api.dto.UrlLinks;

import org.springframework.scheduling.annotation.Async;

public interface CheckConnectionAsynch {
    @Async
    void checkConnection(UrlLinks urlLinks);
}
