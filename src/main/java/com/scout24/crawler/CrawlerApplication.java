package com.scout24.crawler;

import com.scout24.crawler.config.CrawlerAppProps;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableConfigurationProperties(value = CrawlerAppProps.class)
public class CrawlerApplication {

    @Value("${crawler.thread.name:Crawler-}")
    private String crwalerThreadName;

    @Value("${crawler.thread.core.poolsize:7}")
    private int corePoolSize;

    @Value("${crawler.thread.maxpoolsize:42}")
    private int maxPoolSize;

    @Value("${crawler.thread.queueCapacity:11}")
    private int queueCapacity;

    public static void main(String[] args) {
        SpringApplication.run(CrawlerApplication.class, args);
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(crwalerThreadName);
        executor.initialize();
        return executor;
    }

}
